package iut.lp.devmob.objectifssportifs2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import iut.lp.devmob.objectifssportifs2020.database.Firebase;
import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Goal;
import iut.lp.devmob.objectifssportifs2020.model.Sport;
import iut.lp.devmob.objectifssportifs2020.ui.objectifs.ObjectifsFragment;

public class AddActivityActivity extends AppCompatActivity {

    private Spinner sportsSpinner;
    private Button buttonChooseDate;
    private EditText editDuration, editDistance;
    private LinearLayout view_duration, view_distance;
    private DatePickerDialog datePicker;
    FloatingActionButton buttonAddActivity;

    private ProgressDialog progressDialog;

    private ArrayList<Sport> sportsList = new ArrayList<>();
    private ArrayAdapter<Sport> arrayAdapter;
    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    private String distance;
    private String duration;
    private Sport sportSelected;
    private Date dateActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_activity);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.sportsSpinner = (Spinner) findViewById(R.id.spinner_sport);
        this.view_distance = (LinearLayout) findViewById(R.id.view_distance);
        this.view_duration = (LinearLayout) findViewById(R.id.view_duration);
        this.editDistance = (EditText) findViewById(R.id.edit_distance_value);
        this.editDuration = (EditText) findViewById(R.id.edit_duration_value);
        this.buttonAddActivity = (FloatingActionButton) findViewById(R.id.button_add_activity);
        this.buttonChooseDate = (Button) findViewById(R.id.button_date_activity);

        this.view_distance.setVisibility(View.GONE);
        this.view_duration.setVisibility(View.GONE);

        // Spinner settlement with the sports in the database
        this.arrayAdapter = new ArrayAdapter<Sport>(getApplicationContext(), android.R.layout.simple_spinner_item, this.sportsList);
        this.arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.sportsSpinner.setAdapter(this.arrayAdapter);

        // get sports from the database
        firestore.collection("sports").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        sportsList.add(document.toObject(Sport.class));
                    }
                    // spinner update with retrieved sports
                    arrayAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(AddActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                }
            }
        });

        // sport selection and display of the corresponding elements according to the units of obectives accepted by the sport.
        this.sportsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sportSelected = (Sport) sportsSpinner.getSelectedItem();
                distance = sportSelected.getDistance();
                duration = sportSelected.getDuree();

                if (distance.equals("oui")) {
                    view_distance.setVisibility(View.VISIBLE);
                } else {
                    view_distance.setVisibility(View.GONE);
                }
                if (duration.equals("oui")) {
                    view_duration.setVisibility(View.VISIBLE);
                } else {
                    view_duration.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // the date of the activity datePicker
        this.buttonChooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar clrd = Calendar.getInstance();
                int day = clrd.get(Calendar.DAY_OF_MONTH);
                int month = clrd.get(Calendar.MONTH);
                int year = clrd.get(Calendar.YEAR);
                datePicker = new DatePickerDialog(AddActivityActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                clrd.set(Calendar.YEAR,year);
                                clrd.set(Calendar.MONTH,month);
                                clrd.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                                dateActivity = clrd.getTime();

                                String dateActivityString;
                                DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
                                dateActivityString = dateFormat.format(dateActivity);
                                buttonChooseDate.setText(dateActivityString);
                            }
                        },year,month,day);
                datePicker.show();
            }
        });

        this.progressDialog = new ProgressDialog(this);

        // click on the validated button
        this.buttonAddActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String distance = null;
                String duration = null;

                if (sportSelected.getDistance().equals("oui") && !editDistance.getText().toString().equals("")){
                    distance = editDistance.getText().toString();
                }else {
                    distance = null;
                }

                if (sportSelected.getDuree().equals("oui") && !editDuration.getText().toString().equals("")){
                    duration = editDuration.getText().toString();
                }else {
                    duration = null;
                }

                if (sportSelected != null){
                    if (distance != null || duration != null){
                        if (dateActivity != null){
                            addActivity(sportSelected,distance, duration, dateActivity);
                        }else {
                            Toast.makeText(AddActivityActivity.this, R.string.date_activity_required, Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(AddActivityActivity.this,R.string.distance_duration_required,Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(AddActivityActivity.this,R.string.select_sport_required,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     * Add a sport
     * @param sport : the sport concerned by the activity
     * @param distance : the distance value of the activity
     * @param duration : the duration value of the activity
     * @param date : the date of the activity
     */
    private void addActivity(Sport sport, String distance, String duration, Date date) {

        progressDialog.setMessage(getResources().getString(R.string.activity_dialog));
        progressDialog.show();

        ArrayList<Goal> goals = new ArrayList<>();

        Activity activity = new Activity(sport, distance, duration,date);

        // Get objectives with the chosen sport that have not yet been achieved
        firestore.collection("goals")
                .whereEqualTo("sport.libelle", sport.getLibelle())
                .whereEqualTo("reached",false)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (QueryDocumentSnapshot document: task.getResult()){
                            goals.add(document.toObject(Goal.class));
                        }
                    }else {
                        Toast.makeText(AddActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                    }
                });

        // add the activity in the database
        firestore.collection("activities").document(activity.getId()).set(activity)
                .addOnSuccessListener(aVoid -> {
                    progressDialog.dismiss();

                    // completion of the goals corresponding to the chosen sport
                    for (Goal goal : goals){

                        double completion = goal.getCompletion();
                        double newPercentageCompletion = 0;
                        boolean isReached = false;
                        Date startDate = goal.getStartDate();
                        Date endDate = goal.getEndDate();
                        Date activityDate = activity.getDate();
                        if (goal.getDistance() != null){
                            // distance goal
                            if (activity.getDistance() != null && activityDate.after(startDate) && activityDate.before(endDate)){
                                double goalValue = Double.parseDouble(goal.getDistance());
                                completion = completion + Double.parseDouble(distance);
                                newPercentageCompletion = (completion * 100)/goalValue;

                                // when an objective is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this,GoalDetailActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDistance() + " km")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                                isReached = false;
                            }
                            // update of the objective and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(aVoid1 -> {
                                        firestore.collection("activities").document(activity.getId())
                                                .update("alreadyUse",true)
                                                .addOnSuccessListener(aVoid2 -> {

                                                })
                                                .addOnFailureListener(e -> {
                                                    Toast.makeText(AddActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                                });
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(AddActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }else {
                            // duration objectif
                            if (activity.getDuration() != null && activityDate.after(startDate) && activityDate.before(endDate)){
                                double goalValue = Double.parseDouble(goal.getDuration());
                                completion = completion + Double.parseDouble(duration);
                                newPercentageCompletion = (completion * 100)/goalValue;

                                // when an objective is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDuration() + " min")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setCategory(NotificationCompat.CATEGORY_PROMO)
                                            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                                isReached = false;
                            }

                            // update of the objective and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(aVoid1 -> {
                                        firestore.collection("activities").document(activity.getId())
                                                .update("alreadyUse",true)
                                                .addOnSuccessListener(aVoid2 -> {
                                                    Log.v("ACTIVITY","update alreadyuse");
                                                })
                                                .addOnFailureListener(e -> {
                                                    Toast.makeText(AddActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                                });
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(AddActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }
                    }

                    // back to the main activity
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                })
                .addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(AddActivityActivity.this,R.string.error_message, Toast.LENGTH_SHORT).show();
                });

    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}