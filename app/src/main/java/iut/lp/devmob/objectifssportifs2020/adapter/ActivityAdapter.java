package iut.lp.devmob.objectifssportifs2020.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import iut.lp.devmob.objectifssportifs2020.R;
import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Sport;

public class ActivityAdapter extends ArrayAdapter<Activity> {

    public ActivityAdapter(@NonNull Context context, ArrayList<Activity> activities) {
        super(context, 0,activities);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        Activity activity = getItem(position);

        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_list_activity,parent,false);
        }

        TextView activitySport = (TextView) convertView.findViewById(R.id.item_activity_sport);
        TextView activityDate = (TextView) convertView.findViewById(R.id.item_activty_date);
        TextView activityDistance = (TextView) convertView.findViewById(R.id.item_activity_distance);
        TextView activityDuration = (TextView) convertView.findViewById(R.id.item_activity_duration);
        View activityColor = convertView.findViewById(R.id.item_activity_color);

        final Date date = activity.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = dateFormat.format(date);

        activitySport.setText(activity.getSport().getLibelle());
        activityDate.setText(dateString);
        if (activity.getDistance() != null && activity.getSport().getDistance().equals("oui")){
            activityDistance.setText(activity.getDistance() + "km");
        }else{
            activityDistance.setText(" -- km");
        }
        if (activity.getDuration() != null && activity.getSport().getDuree().equals("oui")){
            activityDuration.setText(activity.getDuration() + "min");
        }else {
            activityDuration.setText(" -- min");
        }

        if (activity.getSport().getColor() != null){
            activityColor.setBackgroundColor(activity.getSport().getColor());
        }else {
            activityColor.setBackgroundColor(Color.parseColor("#f2f2f2"));
        }


        return convertView;
    }
}
