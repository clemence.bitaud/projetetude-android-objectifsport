package iut.lp.devmob.objectifssportifs2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import iut.lp.devmob.objectifssportifs2020.adapter.GoalAdapter;
import iut.lp.devmob.objectifssportifs2020.model.Goal;

public class ReachedGoalActivity extends AppCompatActivity {

    private ListView listViewReachedGoals;
    private GoalAdapter goalsAdapter;
    private ArrayList<Goal> goalsReachedList = new ArrayList<>();

    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reached_goal);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.listViewReachedGoals = (ListView) findViewById(R.id.list_reached_goals);
        this.goalsAdapter = new GoalAdapter(this, goalsReachedList);
        this.listViewReachedGoals.setAdapter(goalsAdapter);

        // Retrieving the achieved goals from the database
        firestore.collection("goals").whereEqualTo("reached",true).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        goalsReachedList.add(document.toObject(Goal.class));
                    }
                    goalsAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(ReachedGoalActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}