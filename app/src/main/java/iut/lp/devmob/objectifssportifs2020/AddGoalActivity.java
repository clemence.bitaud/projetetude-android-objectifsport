package iut.lp.devmob.objectifssportifs2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Goal;
import iut.lp.devmob.objectifssportifs2020.model.Sport;

public class AddGoalActivity extends AppCompatActivity {

    private Spinner sportsSpinner;
    private Button buttonStartDate, buttonEndDate;
    private EditText editDurationGoal, editDistanceGoal;
    private LinearLayout view_goal_duration, view_goal_distance, view_radio_button;
    private DatePickerDialog datePickerStart, datePickerEnd;
    FloatingActionButton buttonAddGoal;

    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    private ArrayAdapter<Sport> spinnerAdapter;
    private ProgressDialog progressDialog;
    private ArrayList<Sport> sportsList = new ArrayList<>();
    private Sport sportSelected;
    private Date startDate;
    private Date endDate;
    private Boolean isDistance = false;
    private Boolean isDuration = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_goal);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.sportsSpinner = (Spinner) findViewById(R.id.spinner_sport);
        this.view_goal_distance = (LinearLayout) findViewById(R.id.view_goal_distance);
        this.view_goal_duration = (LinearLayout) findViewById(R.id.view_goal_duration);
        this.view_radio_button = (LinearLayout) findViewById(R.id.view_radio_button);
        this.buttonStartDate = (Button) findViewById(R.id.button_start_date_goal);
        this.buttonEndDate = (Button) findViewById(R.id.button_date_end_goal);
        this.buttonAddGoal = (FloatingActionButton) findViewById(R.id.button_add_goal);
        this.editDistanceGoal = (EditText) findViewById(R.id.edit_distance_goal_value);
        this.editDurationGoal = (EditText) findViewById(R.id.edit_duration_goal_value);

        // Spinner settlement with the sports in the database
        this.spinnerAdapter = new ArrayAdapter<Sport>(getApplicationContext(), android.R.layout.simple_spinner_item, this.sportsList);
        this.spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.sportsSpinner.setAdapter(this.spinnerAdapter);

        // Retrieving sports from the database
        firestore.collection("sports").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        sportsList.add(document.toObject(Sport.class));
                    }
                    // spinner update with retrieved sports
                    spinnerAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(AddGoalActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                }
            }
        });

        // sport selection and display of the corresponding elements according to the units of obectives accepted by the sport.
        this.sportsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sportSelected = (Sport) sportsSpinner.getSelectedItem();
                String distance = sportSelected.getDistance();
                String duration = sportSelected.getDuree();

                if (distance.equals("oui") && duration.equals("oui")){
                    view_goal_distance.setVisibility(View.GONE);
                    view_goal_duration.setVisibility(View.GONE);
                    view_radio_button.setVisibility(View.VISIBLE);
                }else{
                    if (distance.equals("oui")){
                        view_goal_duration.setVisibility(View.GONE);
                        view_radio_button.setVisibility(View.GONE);
                        view_goal_distance.setVisibility(View.VISIBLE);
                    }
                    if (duration.equals("oui")){
                        view_goal_distance.setVisibility(View.GONE);
                        view_radio_button.setVisibility(View.GONE);
                        view_goal_duration.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // the start date datePicker
        this.buttonStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar clrd = Calendar.getInstance();
                int day = clrd.get(Calendar.DAY_OF_MONTH);
                int month = clrd.get(Calendar.MONTH);
                int year = clrd.get(Calendar.YEAR);
                datePickerStart = new DatePickerDialog(AddGoalActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                clrd.set(Calendar.YEAR,year);
                                clrd.set(Calendar.MONTH,month);
                                clrd.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                                startDate = clrd.getTime();

                                String dateStartString;
                                DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
                                dateStartString = dateFormat.format(startDate);
                                buttonStartDate.setText(dateStartString);
                            }
                        },year,month,day);
                datePickerStart.show();
            }
        });

        // the end date datePicker
        this.buttonEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar clrd = Calendar.getInstance();
                int day = clrd.get(Calendar.DAY_OF_MONTH);
                int month = clrd.get(Calendar.MONTH);
                int year = clrd.get(Calendar.YEAR);
                datePickerEnd = new DatePickerDialog(AddGoalActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                clrd.set(Calendar.YEAR,year);
                                clrd.set(Calendar.MONTH,month);
                                clrd.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                                if (startDate.before(clrd.getTime())){
                                    endDate = clrd.getTime();

                                    String dateEndString;
                                    DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
                                    dateEndString = dateFormat.format(endDate);
                                    buttonEndDate.setText(dateEndString);
                                }else {
                                    Toast.makeText(AddGoalActivity.this,R.string.start_date_superior_end_date,Toast.LENGTH_SHORT).show();
                                }

                            }
                        },year,month,day);
                datePickerEnd.show();
            }
        });

        this.progressDialog = new ProgressDialog(this);

        // click on the validated button
        this.buttonAddGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String distanceGoal = null;
                String durationGoal = null;

                if (sportSelected.getDistance().equals("oui") && !editDistanceGoal.getText().toString().equals("")){
                    distanceGoal = editDistanceGoal.getText().toString();
                }else {
                    distanceGoal = null;
                }

                if (sportSelected.getDuree().equals("oui") && !editDurationGoal.getText().toString().equals("")){
                    durationGoal = editDurationGoal.getText().toString();
                }else {
                    durationGoal = null;
                }

                if (sportSelected != null){
                    if (distanceGoal != null && distanceGoal.length() != 0 || durationGoal != null && durationGoal.length() != 0){
                        if (startDate != null && endDate != null){
                            addGoal(sportSelected,startDate,endDate,distanceGoal,durationGoal);
                        }else {
                            Toast.makeText(AddGoalActivity.this, R.string.start_end_date_goal_required, Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(AddGoalActivity.this,R.string.distance_duration_required,Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(AddGoalActivity.this,R.string.select_sport_required,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Add a goal
     * @param sport : the sport concerned by the goal
     * @param startDate : the start date of the goal
     * @param endDate : the end date of the goal
     * @param distance : the target distance value
     * @param duration : the target duration value
     */
    private void addGoal(Sport sport, Date startDate, Date endDate, String distance, String duration){

        progressDialog.setMessage(getResources().getString(R.string.goal_dialog));
        progressDialog.show();

        ArrayList<Activity> activities = new ArrayList<>();

        Goal goal = new Goal(sport,startDate,endDate,distance,duration);

        // get activities with the chosen sport that have not already been used to fulfil an objective
        firestore.collection("activities")
                .whereEqualTo("sport.libelle", sport.getLibelle())
                .whereEqualTo("alreadyUse",false)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (QueryDocumentSnapshot document: task.getResult()){
                            activities.add(document.toObject(Activity.class));
                        }
                    }else {
                        Toast.makeText(AddGoalActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                    }
                });

        // add the objective in the database
        firestore.collection("goals").document(goal.getId()).set(goal)
                .addOnSuccessListener(aVoid -> {
                    progressDialog.dismiss();

                    for (Activity activity : activities){

                        double completion = goal.getCompletion();
                        double newPercentageCompletion;
                        boolean isAlreadyUse = false;
                        boolean isReached = false;
                        Date activityDate = activity.getDate();
                        if (activity.getDistance() != null){
                            // distance activity
                            if (goal.getDistance() != null && activityDate.after(goal.getStartDate()) && activityDate.before(goal.getEndDate())){
                                double goalValue = Double.parseDouble(goal.getDistance());
                                completion = completion + Double.parseDouble(activity.getDistance());
                                goal.setCompletion(completion);
                                newPercentageCompletion = (completion*100)/goalValue;
                                goal.setCompletionPercentage(newPercentageCompletion);
                                isAlreadyUse = true;

                                // when an objective is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this,GoalDetailActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDistance() + " km")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                            }

                            boolean finalIsAlreadyUse = isAlreadyUse;
                            // update of the objective and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(aVoid1 -> {
                                        firestore.collection("activities").document(activity.getId())
                                                .update("alreadyUse", finalIsAlreadyUse)
                                                .addOnSuccessListener(aVoid2 -> {
                                                    Log.v("ACTIVITY","update alreadyuse");
                                                })
                                                .addOnFailureListener(e -> {
                                                    Toast.makeText(AddGoalActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                                });
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(AddGoalActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }else {
                            // duration activity
                            if (goal.getDuration() != null && activityDate.after(goal.getStartDate()) && activityDate.before(goal.getEndDate())){
                                double goalValue = Double.parseDouble(goal.getDuration());
                                completion = completion + Double.parseDouble(activity.getDuration());
                                goal.setCompletion(completion);
                                newPercentageCompletion = (completion*100)/goalValue;
                                goal.setCompletionPercentage(newPercentageCompletion);
                                isAlreadyUse = true;

                                // when an objective is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this,GoalDetailActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDuration() + " min")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                            }

                            boolean finalIsAlreadyUse = isAlreadyUse;
                            // update of the objective and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(aVoid1 -> {
                                        firestore.collection("activities").document(activity.getId())
                                                .update("alreadyUse", finalIsAlreadyUse)
                                                .addOnSuccessListener(aVoid2 -> {
                                                    Log.v("ACTIVITY","update alreadyuse");
                                                })
                                                .addOnFailureListener(e -> {
                                                    Toast.makeText(AddGoalActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                                });
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(AddGoalActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }
                    }

                    // back to the main activity
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                })
                .addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(AddGoalActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                });

    }

    /**
     * Retrieving the click on a checkbox and assigning the action to be performed
     */
    public void onCheckboxGoalTypeClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.checkBox_type_distance:
                if (checked){
                    view_goal_duration.setVisibility(View.GONE);
                    view_goal_distance.setVisibility(View.VISIBLE);
                    isDistance = true;
                    isDuration = false;
                }
                break;
            case R.id.checkBox_type_duration:
                if (checked){
                    view_goal_distance.setVisibility(View.GONE);
                    view_goal_duration.setVisibility(View.VISIBLE);
                    isDuration = true;
                    isDistance = false;
                }
                break;
        }
    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}