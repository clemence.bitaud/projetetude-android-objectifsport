package iut.lp.devmob.objectifssportifs2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Goal;
import iut.lp.devmob.objectifssportifs2020.model.Sport;

public class StartActivityActivity extends AppCompatActivity {

    private Button buttonChooseDate, buttonStart, buttonStop;
    private FloatingActionButton buttonAddActivity;
    private Spinner sportDurationSpinner;
    private Chronometer chronometer;
    private DatePickerDialog datePicker;

    private ArrayList<Sport> sportsList = new ArrayList<>();
    private ArrayAdapter<Sport> arrayAdapter;
    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    private Sport sportSelected;
    private Date dateActivity;
    private String duration;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_activity);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.sportDurationSpinner = (Spinner) findViewById(R.id.spinner_sport_duration);
        this.buttonAddActivity = (FloatingActionButton) findViewById(R.id.button_add_activity);
        this.buttonChooseDate = (Button) findViewById(R.id.button_date_activity);
        this.buttonStart = (Button) findViewById(R.id.button_start_activity);
        this.buttonStop = (Button) findViewById(R.id.button_stop_activity);
        this.chronometer = (Chronometer) findViewById(R.id.simple_chronometer);

        this.chronometer.setBase(SystemClock.elapsedRealtime());

        // Spinner settlement with the sports in the database
        this.arrayAdapter = new ArrayAdapter<Sport>(getApplicationContext(), android.R.layout.simple_spinner_item, this.sportsList);
        this.arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.sportDurationSpinner.setAdapter(this.arrayAdapter);

        // get sports from database with duration unit
        firestore.collection("sports")
                .whereEqualTo("duree","oui")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                sportsList.add(document.toObject(Sport.class));
                            }
                            arrayAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(StartActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                        }
                    }
        });

        // sport selection
        this.sportDurationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sportSelected = (Sport) sportDurationSpinner.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // the date of activity datePicker
        this.buttonChooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar clrd = Calendar.getInstance();
                int day = clrd.get(Calendar.DAY_OF_MONTH);
                int month = clrd.get(Calendar.MONTH);
                int year = clrd.get(Calendar.YEAR);
                datePicker = new DatePickerDialog(StartActivityActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                clrd.set(Calendar.YEAR,year);
                                clrd.set(Calendar.MONTH,month);
                                clrd.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                                dateActivity = clrd.getTime();

                                String dateActivityString;
                                DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
                                dateActivityString = dateFormat.format(dateActivity);
                                buttonChooseDate.setText(dateActivityString);
                            }
                        },year,month,day);
                datePicker.show();
            }
        });

        // the button to start the chronometer
        this.buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
            }
        });

        // the button to stop the chronomter
        this.buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chronometer.stop();
            }
        });

        // calculation of the chronometer time
        this.chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMilli = SystemClock.elapsedRealtime() - chronometer.getBase();
                long minutes = TimeUnit.MILLISECONDS.toMinutes(elapsedMilli);
                duration = String.valueOf(minutes);
            }
        });

        this.progressDialog = new ProgressDialog(this);

        // click on the validated button
        this.buttonAddActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sportSelected != null){
                    if (duration != null && duration.length() != 0){
                        if (dateActivity != null){
                            addActivity(sportSelected,null,duration,dateActivity);
                        }else {
                            Toast.makeText(StartActivityActivity.this, R.string.date_activity_required, Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(StartActivityActivity.this,R.string.chronometer_duration_required,Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(StartActivityActivity.this,R.string.select_sport_required,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Add an activity
     * @param sport : the sport of the activity
     * @param distance : the distance always null
     * @param duration : the duration calculate by the chronometer
     * @param date : the date of the activity
     */
    private void addActivity(Sport sport, String distance, String duration, Date date) {

        progressDialog.setMessage(getResources().getString(R.string.activity_dialog));
        progressDialog.show();

        ArrayList<Goal> goals = new ArrayList<>();

        Activity activity = new Activity(sport, distance, duration,date);

        // Retrieving objectives with the chosen sport that have not yet been achieved
        firestore.collection("goals")
                .whereEqualTo("sport.libelle", sport.getLibelle())
                .whereEqualTo("reached",false)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (QueryDocumentSnapshot document: task.getResult()){
                            goals.add(document.toObject(Goal.class));
                        }
                    }else {
                        Toast.makeText(StartActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                    }
                });

        // add the activity in the database
        firestore.collection("activities").document(activity.getId()).set(activity)
                .addOnSuccessListener(aVoid -> {
                    progressDialog.dismiss();

                    // completion of the goals corresponding to the chosen sport
                    for (Goal goal : goals){

                        double completion = goal.getCompletion();
                        double newPercentageCompletion = 0;
                        boolean isReached = false;
                        Date startDate = goal.getStartDate();
                        Date endDate = goal.getEndDate();
                        Date activityDate = activity.getDate();
                        if (goal.getDistance() != null){
                            // distance goal
                            if (activity.getDistance() != null && activityDate.after(startDate) && activityDate.before(endDate)){
                                double goalValue = Double.parseDouble(goal.getDistance());
                                completion = completion + Double.parseDouble(distance);
                                newPercentageCompletion = (completion * 100)/goalValue;

                                // when a goal is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this,GoalDetailActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDistance() + " km")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                                isReached = false;
                            }
                            // update of the goal and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(aVoid1 -> {
                                        firestore.collection("activities").document(activity.getId())
                                                .update("alreadyUse",true)
                                                .addOnSuccessListener(aVoid2 -> {
                                                    Log.v("ACTIVITY","update alreadyuse");
                                                })
                                                .addOnFailureListener(e -> {
                                                    Toast.makeText(StartActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                                });
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(StartActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }else {
                            // duration goal
                            if (activity.getDuration() != null && activityDate.after(startDate) && activityDate.before(endDate)){
                                double goalValue = Double.parseDouble(goal.getDuration());
                                completion = completion + Double.parseDouble(duration);
                                newPercentageCompletion = (completion * 100)/goalValue;

                                // when a goal is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDuration() + " min")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setCategory(NotificationCompat.CATEGORY_PROMO)
                                            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                                isReached = false;
                            }
                            // update of the goal and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(aVoid1 -> {
                                        firestore.collection("activities").document(activity.getId())
                                                .update("alreadyUse",true)
                                                .addOnSuccessListener(aVoid2 -> {
                                                    Log.v("ACTIVITY","update alreadyuse");
                                                })
                                                .addOnFailureListener(e -> {
                                                    Toast.makeText(StartActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                                });
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(StartActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }
                    }

                    // back to the main activity
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                })
                .addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(StartActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                });
    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}