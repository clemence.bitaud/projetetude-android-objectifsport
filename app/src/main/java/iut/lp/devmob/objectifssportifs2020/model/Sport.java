package iut.lp.devmob.objectifssportifs2020.model;
import java.util.Random;

public class Sport {

    private String id;
    private String libelle;
    private String distance;
    private String duree;
    private Integer color;
    private Random rand = new Random();

    public Sport(){}

    public Sport(String libelle, String distance, String duree, Integer color){
        this.id = String.valueOf(rand.nextInt(100000000-1+1) + 1);
        this.libelle = libelle;
        this.distance = distance;
        this.duree = duree;
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    @Override
    public String toString(){
        return this.libelle;
    }
}
