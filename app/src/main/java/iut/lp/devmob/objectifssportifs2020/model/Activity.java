package iut.lp.devmob.objectifssportifs2020.model;

import java.util.Date;
import java.util.Random;

public class Activity {

    private String id;
    private Sport sport;
    private String duration;
    private String distance;
    private Date date;
    private Random rand = new Random();
    private Boolean isAlreadyUse;

    public Activity(){}

    public Activity(Sport sport, String distance,String duration, Date date){
        this.id = String.valueOf(rand.nextInt(100000000-1+1) + 1);;
        this.sport = sport;
        this.duration = duration;
        this.distance = distance;
        this.date = date;
        this.isAlreadyUse = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuree(String duration) {
        this.duration = duration;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getAlreadyUse() {
        return isAlreadyUse;
    }

    public void setAlreadyUse(Boolean alreadyUse) {
        isAlreadyUse = alreadyUse;
    }


}
