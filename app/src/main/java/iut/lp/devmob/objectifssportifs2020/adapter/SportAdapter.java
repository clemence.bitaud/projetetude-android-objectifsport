package iut.lp.devmob.objectifssportifs2020.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import iut.lp.devmob.objectifssportifs2020.R;
import iut.lp.devmob.objectifssportifs2020.model.Sport;

public class SportAdapter extends ArrayAdapter<Sport> {

    public SportAdapter(@NonNull Context context, ArrayList<Sport> sports) {
        super(context,0,sports);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        Sport sport = getItem(position);

        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_list_sport,parent,false);
        }

        TextView sportName = (TextView) convertView.findViewById(R.id.item_text_sport);
        TextView sportDistance = (TextView) convertView.findViewById(R.id.item_text_distance);
        TextView sportDuration = (TextView) convertView.findViewById(R.id.item_text_duration);
        View sportColor = (View) convertView.findViewById(R.id.item_sport_color);

        sportName.setText(sport.getLibelle());
        sportDistance.setText(sport.getDistance());
        sportDuration.setText(sport.getDuree());


        if (sport.getColor() != null){
            sportColor.setBackgroundColor(sport.getColor());
        }else {
            sportColor.setBackgroundColor(Color.parseColor("#f2f2f2"));
        }

        return convertView;
    }
}
