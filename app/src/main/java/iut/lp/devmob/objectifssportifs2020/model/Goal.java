package iut.lp.devmob.objectifssportifs2020.model;

import java.util.Date;
import java.util.List;
import java.util.Random;

public class Goal {

    private String id;
    private Date startDate;
    private Date endDate;
    private Sport sport;
    private String durationGoal;
    private String distanceGoal;
    private double completion;
    private double completionPercentage;
    private boolean isReached;
    private Random rand = new Random();

    public Goal(){}

    public Goal(Sport sport,Date startDate,Date endDate,String distance,String duration){
        this.id = String.valueOf(rand.nextInt(100000000-1+1) + 1);
        this.startDate = startDate;
        this.endDate = endDate;
        this.sport = sport;
        this.distanceGoal = distance;
        this.durationGoal = duration;
        this.completion = 0;
        this.completionPercentage = 0;
        this.isReached = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public String getDuration() {
        return durationGoal;
    }

    public void setDuration(String duration) {
        this.durationGoal = duration;
    }

    public String getDistance() {
        return distanceGoal;
    }

    public void setDistance(String distance) {
        this.distanceGoal = distance;
    }

    public double getCompletion() {
        return completion;
    }

    public void setCompletion(double completion) {
        this.completion = completion;
    }

    public double getCompletionPercentage() {
        return completionPercentage;
    }

    public void setCompletionPercentage(double completionPercentage) {
        this.completionPercentage = completionPercentage;
    }

    public boolean isReached() {
        return isReached;
    }

    public void setReached(boolean reached) {
        isReached = reached;
    }

}
