package iut.lp.devmob.objectifssportifs2020.ui.objectifs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import iut.lp.devmob.objectifssportifs2020.AddActivityActivity;
import iut.lp.devmob.objectifssportifs2020.AddGoalActivity;
import iut.lp.devmob.objectifssportifs2020.GoalDetailActivity;
import iut.lp.devmob.objectifssportifs2020.R;
import iut.lp.devmob.objectifssportifs2020.ReachedGoalActivity;
import iut.lp.devmob.objectifssportifs2020.UpdateActivityActivity;
import iut.lp.devmob.objectifssportifs2020.adapter.ActivityAdapter;
import iut.lp.devmob.objectifssportifs2020.adapter.GoalAdapter;
import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Goal;

public class ObjectifsFragment extends Fragment {

    private ListView listViewGoals;
    private GoalAdapter goalsAdapter;
    private ArrayList<Goal> goalsList = new ArrayList<>();

    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_objectifs, container, false);

        this.listViewGoals = (ListView) root.findViewById(R.id.list_goals);
        this.goalsAdapter = new GoalAdapter(getContext(), goalsList);
        this.listViewGoals.setAdapter(goalsAdapter);

        // get goals that are not achieved
        firestore.collection("goals").whereEqualTo("reached",false).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        goalsList.add(document.toObject(Goal.class));
                    }
                    goalsAdapter.notifyDataSetChanged();
                    Toast.makeText(getContext(),R.string.delete_goal_toast,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(),R.string.error_message,Toast.LENGTH_SHORT).show();
                }
            }
        });

        // by clicking on a goal in the list you can go to its detail by passing the goal id.
        listViewGoals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent  = new Intent(getActivity(), GoalDetailActivity.class);
                intent.putExtra("goal_id",goalsList.get(position).getId());
                startActivity(intent);
            }
        });

        // on a long click on a goal of the list we delete it
        listViewGoals.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

                // Added vibration for long click
                Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(100);

                String idGoal = goalsList.get(position).getId();

                // deletion confirmation dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(true);
                builder.setTitle(R.string.title_delete_dialog);
                builder.setMessage(R.string.goal_delete_dialog);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // delete the goal from the database
                        firestore.collection("goals")
                                .document(idGoal)
                                .delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        goalsList.remove(position);
                                        goalsAdapter.notifyDataSetChanged();
                                        Toast.makeText(getContext(),R.string.goal_deleted,Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getContext(),R.string.error_message,Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            }
        });

        // add goal button
        Button buttonAddGoal = (Button) root.findViewById(R.id.newGoal);
        buttonAddGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getActivity(), AddGoalActivity.class);
                startActivity(intent);

            }
        });

        // see reached goal button
        Button buttonReachedGoal = (Button) root.findViewById(R.id.button_goal_reached);
        buttonReachedGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ReachedGoalActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }
}