package iut.lp.devmob.objectifssportifs2020;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;

import iut.lp.devmob.objectifssportifs2020.database.Firebase;
import iut.lp.devmob.objectifssportifs2020.model.Sport;
import iut.lp.devmob.objectifssportifs2020.ui.activities.ActivitiesFragment;
import iut.lp.devmob.objectifssportifs2020.ui.sports.SportsFragment;

public class AddSportActivity extends AppCompatActivity {

    private EditText editSportName;
    private Button buttonChooseColor;
    private String distance;
    private String duration;
    private ProgressDialog progressDialog;
    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    private Integer color = null;
    FloatingActionButton buttonAddSport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sport);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.editSportName = (EditText) findViewById(R.id.edit_sport_name);
        this.buttonAddSport = (FloatingActionButton) findViewById(R.id.button_add_sport);
        this.buttonChooseColor = (Button) findViewById(R.id.button_choose_color);

        // the color picker
        this.buttonChooseColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ColorPickerDialogBuilder
                        .with(AddSportActivity.this)
                        .setTitle(R.string.select_color_sport)
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .density(12)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int selectedColor) {
                                color = selectedColor;
//                                Log.d("COLOR", color);
                            }
                        })
                        .setPositiveButton("ok", new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface d, int lastSelectedColor, Integer[] allColors) {
//                                color = Integer.toHexString(lastSelectedColor);
                                color = lastSelectedColor;
                                buttonChooseColor.setBackgroundColor(color);
                                buttonChooseColor.setText("");
                            }
                        })
                        .build()
                        .show();
            }
        });

        this.progressDialog = new ProgressDialog(this);

        // click on the validated button
        this.buttonAddSport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String sportName = editSportName.getText().toString();

                if (sportName != null && sportName.length() != 0){
                    if (distance != null && distance.length() != 0 && duration != null && duration.length() != 0){

                        addSport(sportName, distance,duration, color);

                    }else {
                        Toast.makeText(AddSportActivity.this, R.string.distance_duration_checkbox_required,Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(AddSportActivity.this, R.string.sport_name_required,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     * Add a sport
     * @param sportName : the sport name
     * @param distance : yes if distance sport or no
     * @param duration : yes if duration sport or no
     * @param color : color for the sport
     */
    private void addSport(String sportName, String distance, String duration, Integer color){

        progressDialog.setMessage(getResources().getString(R.string.sport_dialog));
        progressDialog.show();

        Sport sport = new Sport(sportName, distance, duration,color);

        // add the sport in the database
        firestore.collection("sports").document(sport.getId()).set(sport)
                .addOnSuccessListener(aVoid -> {
                    progressDialog.dismiss();

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                })
                .addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(AddSportActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                });
    }

    /**
     * Retrieving the click on the distance yes or no checkbox
     */
    public void onCheckboxDistanceClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.checkBox_distance_yes:
                if (checked){
                    this.distance = "oui";
                }
                break;
            case R.id.checkBox_distance_no:
                if (checked){
                    this.distance = "non";
                }
                break;
        }
    }

    /**
     * Retrieving the click on the duration yes or no checkbox
     */
    public void onCheckboxDurationClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.checkBox_duration_yes:
                if (checked){
                    this.duration = "oui";
                }
                break;
            case R.id.checkBox_duration_no:
                if (checked){
                    this.duration = "non";
                }
                break;
        }
    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}