package iut.lp.devmob.objectifssportifs2020;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Goal;

public class UpdateActivityActivity extends AppCompatActivity {

    private TextView textSportName;
    private Button buttonSportDate;
    private LinearLayout viewUpdateDistance, viewUpdateDuration;
    private EditText editUpdateDistance, editUpdateDuration;
    private Activity activity;
    private FloatingActionButton buttonUpdateActivity;

    private DatePickerDialog datePicker;
    private Date oldOrNewDate;
    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    String idDocumentUpdated;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_activity);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.buttonUpdateActivity = findViewById(R.id.button_update_activity);
        this.buttonSportDate = findViewById(R.id.button_update_date_activity);
        this.idDocumentUpdated = getIntent().getStringExtra("activity_id");

        // Get the activity to update from the database
        firestore.collection("activities").whereEqualTo("id",this.idDocumentUpdated).get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (QueryDocumentSnapshot document: task.getResult()){
                            this.activity = document.toObject(Activity.class);
                        }
                        completeAcitivityInformation();
                    }else {
                        Toast.makeText(UpdateActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                    }
                });

        // the date of the activity datePicker
        this.buttonSportDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar clrd = Calendar.getInstance();
                int day = clrd.get(Calendar.DAY_OF_MONTH);
                int month = clrd.get(Calendar.MONTH);
                int year = clrd.get(Calendar.YEAR);
                datePicker = new DatePickerDialog(UpdateActivityActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                clrd.set(Calendar.YEAR,year);
                                clrd.set(Calendar.MONTH,month);
                                clrd.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                                oldOrNewDate = clrd.getTime();

                                String dateActivityString;
                                DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
                                dateActivityString = dateFormat.format(oldOrNewDate);
                                buttonSportDate.setText(dateActivityString);
                            }
                        },year,month,day);
                datePicker.show();
            }
        });

        this.progressDialog = new ProgressDialog(this);

        // click on the validated button
        this.buttonUpdateActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String distance = null;
                String duration = null;

                if ( !editUpdateDistance.getText().toString().equals("")){
                    distance = editUpdateDistance.getText().toString();
                }else {
                    distance = null;
                }

                if (!editUpdateDuration.getText().toString().equals("")){
                    duration = editUpdateDuration.getText().toString();
                }else {
                    duration = null;
                }

                if (distance != null && distance.length() != 0 || duration != null && duration.length() != 0){
                    if (oldOrNewDate != null){
                        updateAcitvity(oldOrNewDate, distance, duration);
                    }else {
                        Toast.makeText(UpdateActivityActivity.this, R.string.date_activity_required, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(UpdateActivityActivity.this,R.string.distance_duration_required,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * Update the activity
     * @param oldOrNewDate : the old or new date of the activity
     * @param distance : the old or new distance of the activty
     * @param duration : the old or new duration of the activity
     */
    private void updateAcitvity(Date oldOrNewDate, String distance, String duration){

        progressDialog.setMessage(getResources().getString(R.string.update_activity_dialog));
        progressDialog.show();

        ArrayList<Goal> goals = new ArrayList<>();

        // Get objectives with the chosen sport that have not yet been achieved
        firestore.collection("goals")
                .whereEqualTo("sport.libelle", activity.getSport().getLibelle())
                .whereEqualTo("reached",false)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (QueryDocumentSnapshot document: task.getResult()){
                            goals.add(document.toObject(Goal.class));
                        }
                    }else {
                        Toast.makeText(UpdateActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                    }
                });

        // update the activity in the database
        DocumentReference activityRef = firestore.collection("activities").document(idDocumentUpdated);
        activityRef
                .update(
                        "date",oldOrNewDate,
                        "distance",distance,
                        "duration",duration)
                .addOnSuccessListener(aVoid -> {

                    progressDialog.dismiss();

                    // completion of the goals corresponding to the activity sport
                    for (Goal goal : goals){

                        double completion = goal.getCompletion();
                        double newPercentageCompletion = 0;
                        boolean isReached = false;
                        Date startDate = goal.getStartDate();
                        Date endDate = goal.getEndDate();
                        Date activityDate = activity.getDate();
                        if (goal.getDistance() != null){
                            // distance goal
                            if (activity.getDistance() != null && activityDate.after(startDate) && activityDate.before(endDate)){
                                double goalValue = Double.parseDouble(goal.getDistance());
                                completion = completion + Double.parseDouble(distance);
                                newPercentageCompletion = (completion * 100)/goalValue;

                                // when a goal is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this,GoalDetailActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDistance() + " km")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                                isReached = false;
                            }

                            // update of the goal and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(a -> {
                                        Toast.makeText(UpdateActivityActivity.this,R.string.goal_updated,Toast.LENGTH_SHORT).show();
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(UpdateActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }else {
                            // duration goal
                            if (activity.getDuration() != null && activityDate.after(startDate) && activityDate.before(endDate)){
                                double goalValue = Double.parseDouble(goal.getDuration());
                                completion = completion + Double.parseDouble(duration);
                                newPercentageCompletion = (completion * 100)/goalValue;

                                // when a goal is 100% achieved
                                if (newPercentageCompletion >= 100 && goal.isReached() == false){
                                    isReached = true;

                                    // creation of the notification
                                    Intent intent = new Intent(this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Notification")
                                            .setSmallIcon(R.drawable.ic_trophy)
                                            .setContentTitle(getResources().getString(R.string.goal_reached_notification))
                                            .setContentText(goal.getSport().getLibelle() + " - Faire " + goal.getDuration() + " min")
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setCategory(NotificationCompat.CATEGORY_PROMO)
                                            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                                            .setContentIntent(pendingIntent)
                                            .setAutoCancel(true);

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                                    notificationManager.notify(Integer.parseInt(goal.getId()),builder.build());
                                }
                            }else {
                                completion = goal.getCompletion();
                                newPercentageCompletion = goal.getCompletionPercentage();
                                isReached = false;
                            }

                            // update of the goal and activity in the database
                            firestore.collection("goals").document(goal.getId())
                                    .update("completion",completion,
                                            "completionPercentage",newPercentageCompletion,
                                            "reached",isReached)
                                    .addOnSuccessListener(a -> {
                                        Toast.makeText(UpdateActivityActivity.this,R.string.goal_updated,Toast.LENGTH_SHORT).show();
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(UpdateActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                                    });
                        }
                    }

                    // back to the main activity
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                })
                .addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(UpdateActivityActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                });

    }

    /**
     * Completion of the visual elements with the information from the activity
     */
    private void completeAcitivityInformation(){
        this.textSportName = findViewById(R.id.text_update_activity_sport);
        this.viewUpdateDistance = findViewById(R.id.view_update_distance);
        this.viewUpdateDuration = findViewById(R.id.view_update_duration);
        this.editUpdateDistance = findViewById(R.id.edit_update_distance_value);
        this.editUpdateDuration = findViewById(R.id.edit_update_duration_value);

        textSportName.setText(this.activity.getSport().getLibelle());

        oldOrNewDate = activity.getDate();
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        String dateActivityString = dateFormat.format(oldOrNewDate);
        buttonSportDate.setText(dateActivityString);

        if (activity.getSport().getDistance().equals("oui") && activity.getSport().getDuree().equals("oui")){
            this.viewUpdateDistance.setVisibility(View.VISIBLE);
            this.viewUpdateDuration.setVisibility(View.VISIBLE);
            if (this.activity.getDistance() != null){
                this.editUpdateDistance.setText(activity.getDistance());
            }else{
                this.editUpdateDistance.setText("");
            }
            if (this.activity.getDuration() != null){
                this.editUpdateDuration.setText(activity.getDuration());
            }else{
                this.editUpdateDuration.setText("");
            }

        }else{
            if (this.activity.getDistance() != null){
                this.viewUpdateDistance.setVisibility(View.VISIBLE);
                this.editUpdateDistance.setText(activity.getDistance());
            }
            if (this.activity.getDuration() != null){
                this.viewUpdateDuration.setVisibility(View.VISIBLE);
                this.editUpdateDuration.setText(activity.getDuration());
            }
        }
    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}