package iut.lp.devmob.objectifssportifs2020.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import iut.lp.devmob.objectifssportifs2020.R;
import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Goal;

public class GoalAdapter extends ArrayAdapter<Goal> {

    public GoalAdapter(@NonNull Context context, ArrayList<Goal> goals) {
        super(context, 0,goals);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        Goal goal = getItem(position);

        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_list_goal,parent,false);
        }

        TextView goalSport = (TextView) convertView.findViewById(R.id.item_goal_sport);
        TextView goalStartDate = (TextView) convertView.findViewById(R.id.item_goal_start_date);
        TextView goalEndDate = (TextView) convertView.findViewById(R.id.item_goal_end_date);
        TextView goalValue = (TextView) convertView.findViewById(R.id.item_goal_value);
        TextView goalValueUnity = (TextView) convertView.findViewById(R.id.item_goal_value_unity);
        View goalColor = (View) convertView.findViewById(R.id.item_goal_color);

        final Date startDate = goal.getStartDate();
        SimpleDateFormat startDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String startDateString = startDateFormat.format(startDate);

        final Date endDate = goal.getEndDate();
        SimpleDateFormat endDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String endDateString = endDateFormat.format(endDate);

        goalSport.setText(goal.getSport().getLibelle());
        goalStartDate.setText(startDateString);
        goalEndDate.setText(endDateString);

        if (goal.getDistance() != null){
            goalValue.setText(goal.getDistance());
            goalValueUnity.setText(R.string.km);
        }else {
            goalValue.setText(goal.getDuration());
            goalValueUnity.setText(R.string.min);
        }

        if (goal.getSport().getColor() != null){
            goalColor.setBackgroundColor(goal.getSport().getColor());
        }else {
            goalColor.setBackgroundColor(Color.parseColor("#f2f2f2"));
        }

        return convertView;
    }
}
