package iut.lp.devmob.objectifssportifs2020.ui.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import iut.lp.devmob.objectifssportifs2020.AddActivityActivity;
import iut.lp.devmob.objectifssportifs2020.AddSportActivity;
import iut.lp.devmob.objectifssportifs2020.HistoricActivityActivity;
import iut.lp.devmob.objectifssportifs2020.R;
import iut.lp.devmob.objectifssportifs2020.StartActivityActivity;
import iut.lp.devmob.objectifssportifs2020.UpdateActivityActivity;
import iut.lp.devmob.objectifssportifs2020.adapter.ActivityAdapter;
import iut.lp.devmob.objectifssportifs2020.adapter.SportAdapter;
import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Sport;

public class ActivitiesFragment extends Fragment {

    private ListView listViewActivities;
    private ActivityAdapter activitiesAdapter;
    private ArrayList<Activity> activitiesList = new ArrayList<>();

    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_activities, container, false);

        this.listViewActivities = (ListView) root.findViewById(R.id.list_activities);
        this.activitiesAdapter = new ActivityAdapter(getContext(), activitiesList);
        this.listViewActivities.setAdapter(activitiesAdapter);

        // get 20 activities in descending order
        firestore.collection("activities")
                .orderBy("date", Query.Direction.DESCENDING)
                .limit(20)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        activitiesList.add(document.toObject(Activity.class));
                    }
                    activitiesAdapter.notifyDataSetChanged();
                    Toast.makeText(getContext(),R.string.delete_activity_toast,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(),R.string.error_message,Toast.LENGTH_SHORT).show();
                }
            }
        });

        // by clicking on an activity in the list go to update it by passing the activity id.
        this.listViewActivities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent  = new Intent(getActivity(), UpdateActivityActivity.class);
                intent.putExtra("activity_id",activitiesList.get(position).getId());
                startActivity(intent);
            }
        });

        // on a long click on an activity of the list we delete it
        this.listViewActivities.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

                // Added vibration for long click
                Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(100);

                String idActivity = activitiesList.get(position).getId();

                // deletion confirmation dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(true);
                builder.setTitle(R.string.title_delete_dialog);
                builder.setMessage(R.string.activity_delete_dialog);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // delete the activity from the database
                        firestore.collection("activities")
                                .document(idActivity)
                                .delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        activitiesList.remove(position);
                                        activitiesAdapter.notifyDataSetChanged();
                                        Toast.makeText(getContext(),R.string.activity_deleted,Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getContext(),R.string.error_message,Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            }
        });

        Button buttonAddActivity = (Button) root.findViewById(R.id.newActivity);
        buttonAddActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getActivity(), AddActivityActivity.class);
                startActivity(intent);

            }
        });

        Button buttonStartActivity = (Button) root.findViewById(R.id.button_startActivity);
        buttonStartActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), StartActivityActivity.class);
                startActivity(intent);
            }
        });

        Button buttonHistoricActivity = (Button) root.findViewById(R.id.button_activities_history);
        buttonHistoricActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoricActivityActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }

}