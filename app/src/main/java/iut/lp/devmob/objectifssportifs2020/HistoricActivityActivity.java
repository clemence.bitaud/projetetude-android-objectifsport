package iut.lp.devmob.objectifssportifs2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import iut.lp.devmob.objectifssportifs2020.adapter.ActivityAdapter;
import iut.lp.devmob.objectifssportifs2020.model.Activity;

public class HistoricActivityActivity extends AppCompatActivity {

    private ListView listViewHistoricActivities;
    private ActivityAdapter activitiesAdapter;
    private ArrayList<Activity> activitiesHistoricList = new ArrayList<>();

    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic_activity);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.listViewHistoricActivities = (ListView) findViewById(R.id.list_historic_activities);
        this.activitiesAdapter = new ActivityAdapter(this, activitiesHistoricList);
        this.listViewHistoricActivities.setAdapter(activitiesAdapter);

        // get the activities of the database in descending order
        firestore.collection("activities")
                .orderBy("date", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                activitiesHistoricList.add(document.toObject(Activity.class));
                            }
                            activitiesAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(HistoricActivityActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}