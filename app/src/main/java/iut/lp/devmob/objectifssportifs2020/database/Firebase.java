package iut.lp.devmob.objectifssportifs2020.database;

import android.app.ProgressDialog;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import iut.lp.devmob.objectifssportifs2020.model.Sport;

public class Firebase {

    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    public Task<QuerySnapshot> getCollection(String collectionName){
        return firestore.collection(collectionName).get();
    }

    public Task<QuerySnapshot> getDocumentUseWhere(String collectionName, String concernedField, String value){
        return firestore.collection(collectionName).whereEqualTo(concernedField,value).get();
    }

//    public void addSport(Sport sport, @Nullable ProgressDialog progressDialog){
//        firestore.collection("sports").document(sport.getId()).set(sport)
//                .addOnSuccessListener(aVoid -> progressDialog.dismiss())
//                .addOnFailureListener(e -> progressDialog.dismiss());
//    }

}
