package iut.lp.devmob.objectifssportifs2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import iut.lp.devmob.objectifssportifs2020.model.Activity;
import iut.lp.devmob.objectifssportifs2020.model.Goal;

public class GoalDetailActivity extends AppCompatActivity {

    private TextView textSportName,textStartDate, textEndDate,textCompletion, textCompletionGoal,textGoalUnity,textPercentageCompletion;
    private ProgressBar progressBarCompletionPercentage;

    private String idGoalDetail;
    private Goal goal;
    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_detail);

        // add arrow back in the toolbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get the id of the choosed goal in the list passed in the intent
        this.idGoalDetail = getIntent().getStringExtra("goal_id");

        // get the desired goals from the database
        firestore.collection("goals").whereEqualTo("id",this.idGoalDetail).get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (QueryDocumentSnapshot document: task.getResult()){
                            this.goal = document.toObject(Goal.class);
                        }
                        completeGoalDetail();
                    }else {
                        Toast.makeText(GoalDetailActivity.this,R.string.error_message,Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /**
     * Completion of the visual elements with the information from the goal
     */
    private void completeGoalDetail(){

        this.textSportName = findViewById(R.id.text_detail_goal_sport);
        this.textStartDate = findViewById(R.id.text_detail_goal_start_date);
        this.textEndDate = findViewById(R.id.text_detail_goal_end_date);
        this.textCompletion = findViewById(R.id.text_detail_goal_completion);
        this.textCompletionGoal = findViewById(R.id.text_detail_goal_completion_goal);
        this.textGoalUnity = findViewById(R.id.text_detail_value_unity);
        this.textPercentageCompletion = findViewById(R.id.text_detail_goal_percentage_completion);
        this.progressBarCompletionPercentage = findViewById(R.id.progressBar_detail_goal_percentage_completion);

        this.textSportName.setText(this.goal.getSport().getLibelle());

        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

        String startDateGoalString = dateFormat.format(this.goal.getStartDate());
        this.textStartDate.setText(startDateGoalString);
        String endDateGoalString = dateFormat.format(this.goal.getEndDate());
        this.textEndDate.setText(endDateGoalString);

        this.textCompletion.setText(String.valueOf(this.goal.getCompletion()));

        if (this.goal.getDistance() != null){
            this.textCompletionGoal.setText(this.goal.getDistance());
            this.textGoalUnity.setText(" km");
        }else{
            this.textCompletionGoal.setText(this.goal.getDuration());
            this.textGoalUnity.setText(" min");
        }

        this.textPercentageCompletion.setText(String.valueOf((int) this.goal.getCompletionPercentage()));

        Drawable drawable = getResources().getDrawable(R.drawable.customprogressbar);
        this.progressBarCompletionPercentage.setProgressDrawable(drawable);
        this.progressBarCompletionPercentage.setProgress((int)this.goal.getCompletionPercentage());
    }

    /**
     * Action by clicking on the back arrow
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}