package iut.lp.devmob.objectifssportifs2020.ui.sports;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import iut.lp.devmob.objectifssportifs2020.AddSportActivity;
import iut.lp.devmob.objectifssportifs2020.R;
import iut.lp.devmob.objectifssportifs2020.adapter.SportAdapter;
import iut.lp.devmob.objectifssportifs2020.model.Sport;

public class SportsFragment extends Fragment {

    private ListView listViewSport;
    private SportAdapter sportAdapter;
    private ArrayList<Sport> sportList = new ArrayList<>();

    private final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_sports, container, false);

        // set adapter for the list
        this.listViewSport = (ListView) root.findViewById(R.id.list_sports);
        this.sportAdapter = new SportAdapter(getContext(), sportList);
        this.listViewSport.setAdapter(sportAdapter);

        // get sports from the database
        firestore.collection("sports").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        sportList.add(document.toObject(Sport.class));
                    }
                    // indicated the adpater that the data have changed
                    sportAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(),R.string.error_message,Toast.LENGTH_SHORT).show();
                }
            }
        });

        //add sport button
        Button button = (Button) root.findViewById(R.id.click);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getActivity(), AddSportActivity.class);
                startActivity(intent);

            }
        });

        return root;
    }
}