# Application de gestion d'objectifs sportifs

- L'utilisateur peut configurer sa liste de sport : créer autant de sports qu'il le souhaite et pour chaque sport autorisé ou non les objectifs de temps et/ou de distance.
    - Exemple -> nom du sport : Yoga, durée : oui, distance : non
- L'utilisateur peut se créer des objectifs en choisissant un sport, une période et un objectif de temps ou de distance.
    - Exemple -> du 1/11/2020 au 15/11/2020 pour le sport Yoga faire 10h.
- Afin de remplir ses objectifs, l'utilisateur peut saisir manuelement ses activité sportives avec une date, un sport, une durée et/ou une distance.
    - Exemple -> sport: Yoga, date: 2/11/2020, durée: 1h
- L'utilisateur peut consulter la liste des ses activitées, objectifs et sport.
- L'utilisateur peut modifier et supprimer les activité réalisées.
- L'utilisateur peut consulter ses objectifs et voir leur pourcentage de complétion.
- L'utilisateur reçoit une notification quand un objectif est atteint.
